#include "Circle.h"


Circle::Circle(double r){
	setR(r);
}

Circle::~Circle() {

}

void Circle::setR(double r) {
	this->r = r;
}

//void Circle::setR(int r)
//{
//	this->r = r;
//}

bool Circle::isEqual(const Circle& x) {

	return r == x.r;
}

double Circle::getR()const {
	return r;
}

double Circle::calculateCircumference()const {
	return PI * r * 2;
}

double Circle::calculateArea()const {
	return PI * r * r;
}
