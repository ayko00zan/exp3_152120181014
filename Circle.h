#ifndef CIRCLE_H_
#define CIRCLE_H_
class Circle {
public:

	Circle(double);
	virtual ~Circle();
	void setR(double);
	void setR(int);
	double getR()const;
	double calculateCircumference()const;
	double calculateArea()const;
	bool isEqual(const Circle&);
	
private:

	double r;
	const double PI = 3.14;
};
#endif 
